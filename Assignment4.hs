-------------------------------------------------
--                                             --
--   Worksheet for Practical 5, CS316, 2013    --
--                                             --
--   Neil Ghani                                --
--                                             --
-------------------------------------------------
-------------------------------------------------
-- student name:                               --
-- student user id:                            --
-------------------------------------------------

import Data.Char
import Data.Function
import Data.List

-- Question 1: Define the functions add and allEqual 
plusOne :: Int -> Int
plusOne i = i + 1

add :: (Int -> Int) -> Int -> Int
add f 0 = 0
add f i = (f i) + add f (i-1)

-- allEqual :: ??
-- allEqual ?? = ??

-- Question 2: Use fold to define fac and maxList

fold :: (a -> b -> a) -> a -> [b] -> a
fold f a [] = a
fold f a (x:xs) = fold f (f a x) xs

fac :: Int -> Int
fac i = fold (*) 1 [1..i]

maxList :: [Int] -> Int
maxList xs = maximum xs

-- Question 3: Define the function numeral and its inverse 
-- numeral2

numeral :: [Int] -> Int
numeral (x:xs) = fold workInt x xs
   where workInt num s = 10*num + s

numeral2 :: Int -> [Int]
numeral2 i = map digitToInt $ show i

-- Question 4: Define a higher order version of the insertion 
-- sort algorithm
{-

insertBy :: Ord b => (a -> b) -> a -> [a] -> [a] 
insertBy ?? = ??

inssortBy :: Ord b => (a -> b) -> [a] -> [a] 
inssortBy ?? = ??

-}

type Result = (Mark,Mark,Mark)
type Mark = Int



-- Question 5: Define sortTotal

resultsTotal :: Result -> Int
resultsTotal (a,b,c) = (a+b+c)

sortTotal :: [Result] -> [Result]
sortTotal [] = []
sortTotal (x:xs) = sortTotal [y | y <- xs, resultsTotal y >  resultsTotal x] ++ [x] ++ sortTotal [y | y <- xs, resultsTotal y <= resultsTotal x]

-- Question 6: Define sortbyModule

first :: (a, b, c) -> a
first (a, b , c) = a

second :: (a, b, c) -> b
second (a, b , c) = b

third :: (a, b, c) -> c
third (a, b , c) = c

            
sortbyModule :: [Result] -> Int -> [Result]
sortbyModule r i
				| i == 1 = reverse (sortBy (compare `on` first) r)
				| i == 2 = reverse (sortBy (compare `on` second) r)
				| i == 3 = reverse (sortBy (compare `on` third) r)
				| otherwise = r



-- Question 7: Define sortCut
{-
fail :: ??
fail ?? = ??

sortCut :: ??
sortCut ?? = ??

-}

g500 :: [Result]
g500 = [(21,34,56), (43,75,24), (36,95,67),
        (68,23,98), (45,19,46), (68,93,43),
        (16,37,54), (58,96,45), (45,78,67),
        (40,40,40), (37,94,22), (75,42,48),
        (87,56,75), (56,36,64), (46,89,45),
        (34,89,90), (65,75,32), (48,67,67)]






