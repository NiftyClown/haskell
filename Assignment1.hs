type NI = Int
type Age = Int
type Balance = Int
type Person = (NI, Age, Balance)
type Bank = [Person]
type Market = [Bank]
type Pop = [NI]

rbs :: Bank
rbs = [ (1, 73, 1000)
    , (2, 18, -50)
    , (3, 60, 190)
    , (4, 26, 300)
    , (5, 24, 456)
    , (6, 32, 7500)
    , (7, 41, -46)
    , (8, 59, -850)
    , (9, 44, 348)
    , (10, 66, -1000)
    , (11, 37, 20000)
    , (12, 29, -245)
    , (13, 55, 3090)
    ]

barclays :: Bank
barclays = [ (1, 73, 100)
    , (2, 18, 50)
    , (3, 60, 1900)
    , (4, 26, 30)
    , (5, 24, 4565)
    , (6, 32, 750)
    , (7, 41, 1246)
    , (8, 59, -600)
    , (9, 44, -348)
    , (10, 66, 10000)
    , (11, 37, -20000)
    , (12, 29, 247)
    , (13, 55, 30)
    ]

-- The function retired. Defines if a Person
-- is retired (65 & over)
retired :: Person -> Bool
retired (a, b, c) = b >= 65

-- The function deposit. Adds the second input
-- to the Balance of the first input, a Person
deposit :: Person -> Int -> Person
deposit (a, b, c) 0 = error "Please enter a positive number" -- Makes sure the input is not 0
deposit (a, b, c) x = (a, b, c + x) -- I add x to c within the tuple in order to increase the deposit.

-- The function credit. Returns the list of people
-- whos Balance is above 0 (Not overdrawn)
credit :: Bank -> [Person]
credit [] = error "The bank is empty" -- Makes sure the list is not empty
credit a = filter check a -- Apply a filter to the list, a which will return the list using the check function
        where check (x, y, z) = z >= 0 -- Returns true or false if their balance is above 0 (Not overdrawn)

-- The function equityAge returns the total amount
-- of deposits held by people between the two specified
-- ages. Uses map and filter to determine this.
equityAge :: Bank -> (Int, Int) -> Int
equityAge a (0, 0) = error "No ages were selected" -- Makes sure that the ages are not 0
equityAge a (b, 0) = error "Limits out of place" -- Makes sure that b is less than C
equityAge [] (b, c) = 0 -- Empty list, return 0
equityAge a (b, c) = sum [ z | (x, y, z) <- a, y >= b, y <= c] -- Applies a list comprehension to the list, a. Checks the age limits are applied and then uses sum to
																							-- calculate the total of z (the balances of the customers in the bank who meet the criteria)

-- The function creditNI returns the total deposits
-- held by the Person who's NI matches an account in
-- the list of all banks.
creditNI :: NI -> Market -> Int 
creditNI a [] = 0  -- Empty list, return 0
creditNI a b = sum [f | (d, e, f) <- (concat b), d == a] -- Applies a list comprehension to the concatenated list of b (merge the banks into one large bank)
																				   -- It will then test to make sure the national insurance numbers a and d are equal. It will then sum
																				   -- the balances of the accounts who's national insurance number match the input.

-- The function bankFree returns the list of NI numbers that
-- do not hold a bank account
bankFree :: Pop -> Market -> Pop
bankFree [] [] = error "No input length" -- Error, empty lists
bankFree [] b = error "No bank input"
bankFree a [] = error "No NI input"
bankFree a b = filter checkBanks a -- Apply a filter to the list of national insurance numbers, a
        where checkBanks x = not ( any (isBankCustomer x) b) -- We want to return the ones not a customer. 
																								-- Apply the any function to each element of B
																								-- where the isBankCustomer function checks to see
																								-- if each national insurance number, x, is in said bank.
																								-- We then invert the return with not in order to attain those
																								-- who do not have a bank account

isBankCustomer :: NI -> Bank -> Bool
isBankCustomer a [] = False -- Empty list
isBankCustomer a b = any (\(x, _, _) -> x == a) b -- True if the first element is equal to NI. Applies the any function to
																			   -- list b with the case of that is the national insurance number, x, of each
																			   -- tuple in the bank list is equal to the input national insurance number.
																			   -- If there is an occurance, return true.
																			   -- Otherwise, return false.
