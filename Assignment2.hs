import Data.List
import Data.Maybe

--David Forbes
--Barry McAuley
--Agustin Hales

-- Question 1, paper 1

type Word = String
type Line = [Word]
type Book = [Line]

--Test books
harrypotter :: Book
harrypotter = [ ["Hello", "world", "my", "my","is", "name"], ["is", "is", "Harry", "Potter"]]
thehobbit :: Book
thehobbit = [ ["Hobbit", "hobbit", "hobbit", "small","little", "hobbits"], ["fat", "small", "dirty", "baggins's"],["fat","small","hobbits"]]

--Output = [(3,5),(4,6)]
partA :: [(Int, Int)]
partA = [(x, x+2) | x <- [1 .. 4], x > 2]

--Output = [(1,1),(2,2),(5,2),(7,1)]
partB :: [(Int, Int)]
partB = map k [1,2,5,7]
	where k x = (x, x `mod` 3)
	
--Output = 8, it adds phi1, phi2, phi3 to get phi4
partC :: Int
partC = phi 4
	where phi x = 1 + sum [phi y | y <- [1 .. (x -1) ] ]


-- Returns a list of words present in a book
one :: Book -> [Word]
one y = concat y -- Concat the book into a single line, return it

-- Return the amount of times a word occurs in a book
freq :: Word -> Book -> Int
freq x y = sum [(lineSearch x l)| l <- y] -- Sum the list of ints created by the comprehension
										  -- Apply lineSearch to each line of the book.

-- Searche the line for the word and returns
-- the number of times this word appears on said line.
lineSearch :: Word -> Line -> Int
lineSearch x [] = 0 -- Base case
lineSearch x (y:ys) -- Take the head of the line
			| x == y = 1 + lineSearch x ys -- If the word matches, add to the total and recusive call
			| otherwise = lineSearch x ys -- If not, recursive call on the tail

-- Takes in a book and returns a list of a pair of words and the number of lines
-- they occur on
bookFreq :: Book -> [(Word, Int)]
bookFreq b = nub [(w, freq w z) | x <- z, w <- x] -- nub removes the duplicates. W is a Word from X which is a list from the nubbed Book.
		where z = map nub b -- Maps nub to remove the duplicates from the book

-- Checks the line for the occurance of a word
-- If present, returns true. If not, returns false.
lineCheck :: Word -> Line -> Bool
lineCheck w [] = False -- Base case
lineCheck w (l:ls)
			| w == l = True -- If the word is == to the head of the line, return True
			| otherwise = lineCheck w ls -- Otherwise, work on the rest of the list until we hit the base case

-- Find the index of each line a word appears on
index :: Word -> Book -> [Int]
index w b = [ fromJust(elemIndex x b) + 1 | x <- b, lineCheck w x] -- Gets the index of the line x if the word is present