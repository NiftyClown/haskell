-------------------------------------------------
--                                             			  	--
--   Worksheet4
--                                             --
--   Author: Neil Ghani                        --
-------------------------------------------------
-- Barry McAuley, Agustin Hales and David Forbes --
-- 201106816, 201107341, 201145420                              -- 
-------------------------------------------------

import Data.List
import Data.Maybe

-- Question 1: Define the function addup using 
-- recursion. Ensure an error is flagged for 
-- negative input

addup :: Int -> Int --take an int as input and output an int
addup 0 = 0 --test the base case
addup x = x + addup (x - 1) --we recursively addup x sum of 
                            --natural number which is less 
                            --than or equal to input.

-------------------------------------------------

-- Question 2: Define the function copy using 
-- recursion. Define the function copy2 using 
-- list comprehension and not recursion

copy :: Int -> a -> [a] --takes int as input and element of a type and returns list.
copy 0 i = [] --test fot the base case
copy x i = i : copy (x-1) i --we apply copy recursively to i then outputs it as a list.
--the -1 is so that it terminates. 

copy2 :: Int -> a -> [a] --takes int and element as input and returns a list
copy2 x i = [i | y <- [1 .. x],  y >= 0] --we use a list comprehension so do the same as copy 
--but in copy2 we dont use recursion.

------------------------------------------------

-- Question 3: Use list recursion to define the 
-- function addone which adds one to every element 
-- in a list of integers. Define the function addone2 
-- using map instead of recursion

addone :: [Int] -> [Int] --take a list as an input and returns a list
addone [] = [] --test for an empty list (base case)
addone (x:xs) = (x+1) : addone xs --we recursively add 1 to every element of the list.

addone2 :: [Int] -> [Int] --same as addone but we use map to add one to every element of the list.
addone2 x = map a x
                where a b = b+1
-----------------------------------------------

-- Question 4: Define the function maxList using 
-- list recursion
 
maxList :: [Int] -> Int --takes a list as input and outputs an int
maxList [] = 0 --test the empty list which returns a value.
maxList (x:xs) = set x xs --we set a function which will return the max value of a list.
                where
                                set m [] = m
                                set m (y:ys) 
                                                        | m > y = set m ys --we use gaurds to test if m value is greater than y value 
                                                                           --if it is then set it if not the return the greater value.
                                                        | otherwise = set y ys
 
 --------------------

-- Question 5: Define the function join using list 
-- recursion

join :: [[a]] -> [a] --we take in a list of lists as input and output a list 
join [] = [] --test for empty list(base case)
join (x:xs) = x  ++ (join xs) --we join all lists together.

--the reason the output is a list its because its a list of characters.

-----------------------------------------------
--                                           --
--     Electronic Hand in Questions          --
--                                           --
-----------------------------------------------


-- Question 6: Define the functions rev1, rev2 and 
-- rev3

rev1 :: [a] -> [a] --takes list as input and outputs a list
rev1 [] = [] --test for the empty list (base case)
rev1 xs = last xs : rev1 (init xs) --this function uses recusion to append the head of a list 
                                   --to the end of the result of reversing the tail of ther list.

rev2 :: [a] -> [a] --takes list as input and outputs a list 
rev2 [] = [] --test fot the empty list (base case)
rev2 xs = rev1 (g xs) ++ rev1 ( f xs)  --here we slipt the list in half and call rev1 to reverse each half
                where 
                        g y = drop (length y `div` 2) y 
                        f y = take (length y `div` 2) y

rev3 :: [a] -> [a] -> [a] --takes two lists as input and outputs a list
rev3 xs ys = rev1 (xs ++ ys) -- we call rev1 to reverse the the added lists.


---------------------------------------------------
-- Pontoon attack!!!
---------------------------------------------------

type Value = String
type Suit = String
type Card = (Value, Suit)
type Pack = [Card]

--create list of suits
suits::[Suit]
suits = ["Spades", "Clubs", "Hearts", "Diamond"]

--create a list of values
values :: [Value]
values = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", 
          "Nine", "Ten", "Jack", "Queen", "King"]


pack :: Pack --just returns pack
pack = [ (x, y) | x <- values, y <- suits] --pack is a combination of suit and value and returns all of them with in a pack.

val :: Card -> [Int] --takes in card as input which is pair and outputs a list
val (x,y)
        | x == head values = [1,11] 
        | (fromJust(elemIndex x values)+1) > 10 = [10]--we use the fromJust function which extracts the elements
        | otherwise = [fromJust(elemIndex x values)+1]

-------------------------------------------------------------------
-- Question 7: Define the functions interleave, shuffle 
-- and shuffleList

interleave :: [a] -> [a] -> [a] --takes two lists as input and outputs a list
interleave [] [] = [] --test for two empty lists 
interleave x [] = x --test form one empty list
interleave [] y = y --test from the other empty list
interleave (x:xs) (y:ys) = x : y : interleave xs ys --which will take the first element of list 1 
                                                    --then map the first element of the other list we do this recursively. 

shuffle :: Pack -> Int -> Pack --takes pack and int as input
shuffle p i = interleave (take i p) (drop i p) --this cuts up the deck and interleave each half  

shuffleList :: Pack -> [Int] -> Pack --takes pack and a list of ints as input and outputs the pack
shuffleList p [] = p --test for the empty list but return a value rather than an error
shuffleList p (i:is) = shuffleList (shuffle p i) is --we recursibly call shuffleList on the pack obtained by applying shuffle

--------------------------------------------------------------------

type Hand = [Card]

-- Question 8: Define the functions handValue which 
-- returns the value of a list of cards and the function 
-- bust

testHand :: Hand
testHand = [("King", "Spades"), ("King", "Clubs"), ("King", "Diamond")]

-- Takes the cartesian product of two lists of ints
cartProd :: [Int] -> [Int] -> [Int] 
cartProd xs ys = nub [ x+y | x <- xs, y <- ys] -- Removes the duplicates in the list and returns the cartesian product

-- Splits the hand into seperate lists of the cards values
splitHand :: Hand -> [[Int]] 
splitHand [] = [] -- Base case
splitHand (x:xs) = [val x] ++ splitHand xs -- Stitches together the lists using ++ and recursively calls

-- Takes the cartesian product of multiple lists
cartMulti :: [[Int]] -> [Int] 
cartMulti [] = [0] -- Base case
cartMulti (x:xs) = cartProd x b -- Take the cartesian product 
		where b = cartMulti xs -- Recursively call

-- Returns the value of the hand the player has
handValue :: Hand -> [Int] 
handValue [] = [] -- Base case
handValue xs = filter test (cartMulti x) -- Filter the results and apply the cartesian product
		where 
			x = splitHand xs -- Split the hand up
			test y = y < 22 -- Filter those above 22

-- Tests if a hand is bust
bust :: Hand -> Bool
bust h = handValue h == [] && h /= [] -- If handValue returns an empty list, it's bust. Catch if hand is empty.

-----------------------------------------------------------------------
-- Question 9: Define the function twist16

type Player = Hand->Bool

-- Decides if a player should twist or not
twist16 :: Player
twist16 h = (maxList (handValue h) < 17) && (bust h == False) -- If the max value of a hand is less than 17 and the hand isnt bust


---------------------------------------------------------------
-- Question 10: Define the functions play and game

-- Play function keeps adding cards to the players hand until they should not twist anymore
play :: Player -> (Hand,Pack) -> (Hand,Pack)
play p (h, (k:ks))
		| p h = play p (k:h, ks) -- If they should twist, keep playing and add the top of the deck to their hand
		| otherwise = (h, k:ks) -- Return the final hand and the deck
		
---------------------------------------------------------------
-- Question 11: Define the functions play and game

-- Plays a game of pontoon
game :: Pack -> [Player] -> [Hand]
game [] [] = [] --
game [] x = [] -- Base cases
game p [] = [] --
game p (x:xs) = [ fst (play x ([ ], p)) ] ++ game (snd(play x ([ ], p))) xs -- First, takes the first of the (Hand,Deck) tuple returned by
									-- the play function and this is wrapped in a list. It will then append
									-- this list to the recursive call of the function containing the edited pack
									-- which we extract using the snd funciton of the play call we made previously
									-- and the rest of the players.

------------------------------------------------------------- 
--                                                         --
-- Remove the comment delimiters once you have written the -- 
-- functions game and handValue                            -- 
--                                                         --
-------------------------------------------------------------

showHand :: [Hand] -> IO()
showHand h = putStr (foldl (\x y -> x ++ "\n" ++ format y) [] h 
                     ++ "\n"  
                     ++ "Winner is player" 
                     ++ show (winner h))

format :: Hand -> String
format [] = []
format ((x1,y1):cs) = x1 ++ "\t" ++ y1 ++ "\n" ++ format cs 

showGame :: Pack -> [Player] -> IO()
showGame p s = showHand (game p s)  

winner :: [Hand] -> Int
winner [] = 0
winner (h:hs) 
   | mL (handValue h) > mL (concat (map handValue hs)) = 1
   | otherwise = winner hs + 1
     where mL xs = foldl max 0 xs
