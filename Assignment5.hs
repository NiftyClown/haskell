---------------------------------------------------------------------
-- CS316: Propositional Logic
-- File: CW06.hs
---------------------------------------------------------------------
-- Barry McAuley, David Forbes and Agustin Hales
-- 201106816, 201145420 
---------------------------------------------------------------------

type Variable  = String
type Valuation = [(Variable, Bool)] -- Valuation of variables to truth values 

-- A (propositional) formula is
data Prop = Falsum         -- a contradiction, or
          | Var Variable   -- a variable, or
          | Not Prop       -- a negation of a formula, or
          | Or  Prop Prop  -- a disjunction of two formulae, or
          | And Prop Prop  -- a conjunction of two formulae, or
          | Imp Prop Prop  -- a conditional of two formulae.
            deriving (Eq, Show)

-- A running example
p = Var "p"
q = Var "q"
a = Var "a"
b = Var "b"

example = And p (Or q (Not q))
example2 = And p (Not p)
example3 = Or p (Not p)
example4 = Or (And a (Not b)) (And a q)

dis = And p (Not q)
dis2 = And a (Not b)
-- First, we define an auxiliary function that removes duplicates from
-- a given list. E.g., nub [2,5,2,1,2,5] == [2,5,1]

nub :: Eq a => [a] -> [a]
nub = nub' []
  where
    nub' _    []        = []
    nub' seen (x:xs)
        | x `elem` seen = nub' seen xs
        | otherwise     = x : nub' (x:seen) xs


---------------------------------------------------------------------
-- Question 1: Explain what the three functions "vars", "eval" and
-- "valuation" do.  Precede the lines by hyphons so that ghci treats
-- them as comments.
--
--


-- Vars takes any given proposition then
-- evaluates it in order to distiguish which
-- variables are present within the proposition 
-- then returns them in a list.
vars :: Prop -> [Variable]
vars = nub . vars'
    where
      vars' Falsum    = []
      vars' (Var v)   = [v]
      vars' (Not f)   = vars' f
      vars' (Or  f g) = vars' f ++ vars' g
      vars' (And f g) = vars' f ++ vars' g
      vars' (Imp f g) = vars' f ++ vars' g

-- The function eval takes in a list on inputs in the form 
-- of valuations and then applies each of these arguments to the
-- proposition given in the second input. It then computates
-- the answer based on the input to the function.
eval :: Valuation -> Prop -> Bool
eval val Falsum    = False
eval val (Var v)   = case (lookup v val) of
                       Nothing -> error ("Unbound variable: " ++ v)
                       Just t  -> t
eval val (Not f)   = not (eval val f)
eval val (Or  f g) = (eval val f) || (eval val g)
eval val (And f g) = (eval val f) && (eval val g)
eval val (Imp f g) = eval val (Or (Not f) g)

-- Valuations returns the truth table according to the number
-- variables given in the list.
valuations :: [Variable] -> [Valuation]
valuations []     = [[]]
valuations (v:vs) = map ((v,True):) ds ++ map ((v,False):) ds 
    where ds = valuations vs


---------------------------------------------------------------------
-- Question 2: Define a function that given a formula returns the
-- models of that formula, where a model is a valuation which makes
-- the formula true. E.g.,
-- models example == [[("p",True),("q",True)],[("p",True),("q",False)]]

models :: Prop -> [Valuation]
models p = filter function (valuations (vars p))
      where function y = eval y p 

---------------------------------------------------------------------
-- Question 3: A contradiction is a proposition that is always
-- false. A tautology is a proposition that is always true. Define
-- these two functions below.

contradiction :: Prop -> Bool
contradiction x = models(x) == []

tautology :: Prop -> Bool
tautology x = models(x) == valuations(vars x)


-- Below is a function that transforms any propositional formula to
-- its Disjunctive Normal Form (DNF): The DNF of a contradiction is
-- Falsum. A formula, that is not a contradiction, is in disjunctive
-- normal form if it is a disjunction of conjunctions where every
-- variable or its negation is represented exactly once in each
-- conjunction. The individual conjunctions that make up the DNF are
-- called minterms. For instance, the DNF of "example" is
--
-- Or (And (Var "p") (Var "q")) (And (Var "p") (Not (Var "q")))
--
-- and it has two minterms, namely (And (Var "p") (Var "q")) and
-- (And (Var "p") (Not (Var "q")))
--
-- Your job is to define "disjunction" and "minterm".

-- Uncomment this function when you have done Q4 and Q5.
-- Then, try
-- dnf (Imp p q) == dnf (Or (Not p) (q))      and
-- dnf (And p (Not p)) == Falsum
-- They should both return True.
--
dnf :: Prop -> Prop
dnf p = case models p of
           [] -> Falsum
           ms -> disjunction (map minterm ms)


---------------------------------------------------------------------
-- Question 4: Define a function that given a nonempty list of
-- formulae returns the disjunction of them, as in
-- disjunction [f0, f1, f2] == Or f0 (Or f1 f2)


disjunction :: [Prop] ->  Prop
disjunction [x] = x
disjunction (x:xs) = Or x (disjunction xs) 

---------------------------------------------------------------------
-- Question 5: Define a function that given a valuation (a row in the
-- truth table) returns a conjunction with one term per variable, each
-- of which is (Var v) if v is True and (Not (Var v)) if v is False. 
-- E.g.,
-- minterm [("p",True),("q",False)] == And (Var "p") (Not (Var "q"))

minterm :: Valuation -> Prop
minterm [(x1, x2)]
                | x2 = Var x1
                | otherwise = Not (Var x1)
minterm ((x1, x2):xs)
                | x2 = And (Var x1) (minterm xs)
                | otherwise = And (Not (Var x1)) (minterm xs)