--------------------------------------------------------------------------------
-- CS316: An Imperative Language
-- File: CW07.hs
--------------------------------------------------------------------------------
-- Student Name 		-- David Forbes --   
-- Student User Id		-- 201145420      --
--------------------------------------------------------------------------------

import Prelude hiding (lookup)

-- Arithmetic expressions
data AExpr = Num Int
           | Add AExpr AExpr
           | Sub AExpr AExpr
           | Var String
            deriving (Show, Eq)

-- Boolean expressions
data BExpr = Boo Bool
           | LEq AExpr AExpr
           | GEq AExpr AExpr
           | Not BExpr
           | And BExpr BExpr 
            deriving (Show, Eq)

--data Command = While AExpr Command
 --            | Assign String AExpr
 --            | If AExpr Command Command
 --            | Seq Command Command
 --            | Pass
 --          deriving (Show, Eq)

data Command = While BExpr Command
              | Assign String AExpr
              | If BExpr Command Command
              | Seq Command Command
              | Inc String
              | Pass
             deriving (Show, Eq)

type Memory = [(String, Int)]

lookup :: Memory -> String -> Maybe Int
lookup []         _ = Nothing
lookup ((s,n):ms) t = if s == t then Just n else lookup ms t

update :: Memory -> (String, Int) -> Memory
update [] (t,k) = [(t,k)]
update ((s,n):ms) (t,k)
    | s == t    = (s,k) : ms
    | otherwise = (s,n) : update ms (t,k)

aEval :: Memory -> AExpr -> Int
aEval _ (Num n)      = n
aEval m (Add e1 e2)  = aEval m e1 + aEval m e2
aEval m (Sub e1 e2)  = aEval m e1 - aEval m e2
aEval m (Var x)      = case lookup m x of
                         Just v  -> v
                         Nothing -> error $ "Unbound variable: " ++ show x


--------------------------------------------------------------------------------
-- Question 1: Implement bEval.
--
bEval :: Memory -> BExpr -> Bool
bEval _ (Boo n)       = n
bEval m (LEq e1 e2)   = if((aEval m e1) < (aEval m e2)) then True else False
bEval m (GEq e1 e2)   = if((aEval m e1) > (aEval m e2)) then True else False
bEval m (Not e1)      = if(bEval m e1 == True) then  False else True
bEval m (And e1 e2)   = if((bEval m e1 == True) && (bEval m e2 == True)) then True else False


--------------------------------------------------------------------------------
-- Question 2: Replace the old Command def with the new and update exec.
--
exec :: Memory -> Command -> Memory
exec m (While e c)  = case bEval m e of
                        False -> m
                        _ -> let m' = exec m c in exec m' (While e c)
exec m (Assign s e) = update m (s, aEval m e)
exec m (If e c1 c2) = case bEval m e of
                        False -> exec m c2
                        _ -> exec m c1
exec m (Seq c1 c2)  = let m' = exec m c1 in exec m' c2
exec m (Inc s)     = case lookup m s of
                         Just v  -> update m (s, v+1 )
                         Nothing -> error $ "Unbound variable: " ++ show s 
exec m Pass         = m

-- Sequentially compose a sequence of commands
prog :: [Command] -> Command
prog = foldl1 Seq

-- Run a command with memory initially empty
run :: Command -> Memory
run = exec []


--------------------------------------------------------------------------------
-- Question 3: Define equal and notEqual
--
-- Thats was to easy not sure if right
--
equal :: AExpr -> AExpr -> BExpr
equal e1 e2 = if(e1 == e2) then (Boo True) else (Boo False)

notEqual :: AExpr -> AExpr -> BExpr
notEqual e1 e2 = if(e1 /= e2) then (Boo True) else (Boo False)


--------------------------------------------------------------------------------
-- Hi guys i have done upto here !!!
--
-- Question 4: Implement the following program
--
--    a := 1071
--    b := 462
--    while a /= b {
--       if a >= b
--          a := a - b
--       else
--          b := b - a
--    }
--
-- Run the program. What is the result? (Do you know why?)
q4 :: Command
q4 = undefined

--------------------------------------------------------------------------------
-- Question 5: Define a for-loop command. It should capture this
-- pseudo-code:
--
--    for s := e1 to e2 {
--      c
--    }
--
for :: String -> AExpr -> AExpr -> Command -> Command
for s e1 e2 c = undefined

--------------------------------------------------------------------------------
-- Question 6: Implement the following program
--
--    n := 5
--    nsqr := 0
--    for i := 1 to n {
--       nsqr := nsqr + n
--    }
--
-- Display the abstract syntax of your program by typing its name on the
-- ghci prompt. Write down this program in pseudo-code (it will contain
-- a while looop) and convince yourself that it does the same as the
-- for-loop version above.
--
sqr :: Command
sqr = undefined
